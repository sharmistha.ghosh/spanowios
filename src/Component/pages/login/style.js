import { Dimensions } from "react-native";
let ScreenHeight = Dimensions.get("window").height;
export default {
  bodymain: {
    flex: 1,
    minHeight: ScreenHeight,
    paddingTop: 30,
    alignSelf: "stretch",
    height: "110%"
  },
  container: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 15
  },
  logo: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 40,
    marginBottom: 30
  },
  heading: {
    fontSize: 20,
    color: "#fff",
    textAlign: "center",
    marginBottom: 30
  },
  forminput: {
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#934bb5",
    padding: 8,
    height: 45,
    width: "100%",
    color: "#fff",
    fontSize: 15,
    marginBottom: 8
  },
  pinktext: {
    color: "#e25cff",
    fontSize: 18,
    textAlign: "center"
  },
  forgetpass: {
    //padding:10,
    marginBottom: 40,
    //height:50,
    alignItems: "center"
  },
  pinkbtn: {
    backgroundColor: "#e25cff",
    height: 45,
    lineHeight: 45,
    width: "100%",
    padding: 10,
    marginBottom: 20,
    justifyContent: "center",
    borderRadius: 5
  },
  btntext: {
    fontSize: 15,
    color: "#fff",
    textAlign: "center"
  },
  aplebtntext: {
    fontSize: 15,
    textAlign: "center"
  },
  orline: {
    width: "100%",
    height: 30,
    marginBottom: 25
  },
  googlebtn: {
    width: "45%",
    backgroundColor: "#3b5998",
    height: 45,
    lineHeight: 45,
    padding: 10,
    justifyContent: "center",
    borderRadius: 5
  },
  applebtn: {
    width: "45%",
    backgroundColor: "#ffffff",
    height: 45,
    lineHeight: 45,
    padding: 10,
    justifyContent: "center",
    alignItems: 'center',
    borderRadius: 5,
  },
  appleView: {
    width: "100%", 
    justifyContent: 'center', 
    alignItems: 'center',
    marginBottom: 20
  },
  fbbtn: {
    width: "45%",
    backgroundColor: "#1c4297",
    height: 45,
    lineHeight: 45,
    padding: 10,
    justifyContent: "center",
    borderRadius: 5
  },
  jsbtn: {
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 25,
    flexDirection: "row"
  },
  bttext: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 25,
    flexDirection: "column"
  },
  text: {
    fontSize: 18,
    color: "#fff"
  },
  ThankYou: {
    height: "100%",
    flex: 1,
    width: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  graycontainer: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingBottom: 15,
    backgroundColor: "#1a1a1d",
    paddingTop: 20
  },
  topbar: {
    backgroundColor: "#303038",
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 15,
    paddingRight: 15,
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row"
  },
  error: {
    fontSize: 16,
    color: "#FF0000",
    paddingBottom: 22
  },
  disableFbButton: {
    width: 136,
    borderRadius: 6,
    fontSize: 10,
    height: 46,
    backgroundColor: '#B2BEB3',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  popupText:{
    color: "#000000",
    fontWeight: "bold",
    marginTop: 4
  }
};
