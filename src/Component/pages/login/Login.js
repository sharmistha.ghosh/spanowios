import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ImageBackground,
  AsyncStorage,
  Image,
  TextInput,
  Alert
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import styles from "./style";
import Spinner from "react-native-loading-spinner-overlay";
import { Button, CheckBox } from "react-native-elements";

import { connect } from "react-redux";
import { login, fbLogin, googleLogin, appleLogin } from "./redux/authAction";
//import AsyncStorage from '@react-native-community/async-storage';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import { throwStatement } from "@babel/types";
import {
  LoginButton,
  AccessToken,
  LoginManager,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';
import appleAuth, {
  AppleButton,
  AppleAuthRequestOperation,
  AppleAuthRequestScope,
  //AppleAuthCredentialState,
} from '@invertase/react-native-apple-authentication';
import moment from "moment";


GoogleSignin.configure({
  //scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
  iosClientId: '1060175552013-9tg0oneascm5j44gql5ebup6acn7l6j2.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
  webClientId: '1060175552013-p922bb9uou4nbvr9evk43jc0tckhd5ao.apps.googleusercontent.com',
  offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
  hostedDomain: '', // specifies a hosted domain restriction
  loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
  forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
  accountName: '', // [Android] specifies an account name on the device that should be used
});


class LoginScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      first_name: "",
      first_name_error: "",
      last_name: "",
      last_name_error: "",
      phone: "",
      phone_error: "",
      email: "",
      password: "",
      email_error: "",
      password_error: "",
      login_userid: "",
      spinner: false,
      visible1: false,
      popupMsg: "",
      fb_signup_id: "",
      name: "",
      checkbox: false,
      mark: 1,
      facebookButton: 1,
      visible3: false,
      isSigninInProgress: false,
      gettingLoginStatus: true,
      userInfo: null,
    };
  }

  faceBookLogin = async () => {
    LoginManager.logOut();
    console.log("faceBookLogin");
    try {
    const userInfo = await LoginManager.logInWithPermissions(["public_profile", "email"]);
    console.log(userInfo);
    if (userInfo.isCancelled) {
      console.log("Login cancelled");
    } else {
      console.log(
        "Login success with permissions: " +
        userInfo
      );
      AccessToken.getCurrentAccessToken().then(
        (data) => {
          let accessToken = data.accessToken.toString();
          console.log("accesstoken",accessToken);
          if(accessToken) {
            fetch('https://graph.facebook.com/v2.5/me?fields=email,name,picture,friends&access_token=' + accessToken)
            .then((response) => response.json())
            .then((json) => {
              console.log("userdetails",json);

              let email = json.email ? json.email : "";
              let name = json.name;
              let fbAccessId = json.id;
              this.fetchFbData(email,name,fbAccessId);
            })
            .catch(() => {
              reject('ERROR GETTING DATA FROM FACEBOOK')
            })
          }
        }
      )
    }
  } catch (error) {
    console.log("signin error",error);
    Toast.show(I18n.t("Something went wrong!"));
  }
  }


  fetchFbData = (email,name,fbAccessId) => {
    let first_name;
    let last_name;
    if(name.includes(" ")){
      first_name = name.split(" ")[0];
      last_name = name.split(" ")[1];
    }
    else{
      first_name = name;
    }
    let currenttime = moment(new Date()).format("hh:mm:ss");
    let todayDate = moment(new Date()).format("YYYY-MM-DD");
    console.log("token",email);
        this.setState({ spinner: true });
        let facebookDetails = new FormData();
        facebookDetails.append("first_name", first_name);
        facebookDetails.append("last_name", last_name);
        facebookDetails.append("name", name);
        facebookDetails.append("email", email ? email : "");
        facebookDetails.append("fb_signup_id", fbAccessId);
        facebookDetails.append("todaysDate", todayDate);
        facebookDetails.append('time', currenttime),
        
        this.props.fbLogin(facebookDetails);
        console.log("facebookDetails",facebookDetails);
  }

  signIn = async () => {
    this.signInFirst();
    // const isSignedIn = await GoogleSignin.isSignedIn();
    // console.log("isSignedIn",isSignedIn);
    // if(isSignedIn == true){
    //   this.signInSilent();
    // }
    // else{
    //   this.signInFirst();
    // }
  };

  signInSilent = async () => {
    console.log("signInSilent");
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log("signInSilently userInfo",userInfo);
      let currenttime = moment(new Date()).format("hh:mm:ss");
      let todayDate = moment(new Date()).format("YYYY-MM-DD");
      let googleDetails = new FormData();
      googleDetails.append('first_name', userInfo.user.givenName);
      googleDetails.append('last_name', userInfo.user.familyName);
      googleDetails.append('name', userInfo.user.name);
      googleDetails.append('email', userInfo.user.email);
      googleDetails.append('photo', userInfo.user.photo);
      googleDetails.append('google_signup_id', userInfo.user.id);
      googleDetails.append("todaysDate", todayDate);
      googleDetails.append('time', currenttime);

      if(userInfo.user.email !=''){
        this.setState({spinner: true})
        this.props.googleLogin(googleDetails);
      }
      else{
        Alert.alert("Sorry. Some problem occurs. Please try again in some time.")
      }
    } catch (error) {
      console.log("hhhhhhhh",error);
    }
  };

  signInFirst = async () => {
    console.log("signInFirst");
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log("signInFirst userInfo", userInfo);

      var google_data = JSON.stringify(userInfo);
      let currenttime = moment(new Date()).format("hh:mm:ss");
      let todayDate = moment(new Date()).format("YYYY-MM-DD");
      let googleDetails = new FormData();
      googleDetails.append('first_name', userInfo.user.givenName);
      googleDetails.append('last_name', userInfo.user.familyName);
      googleDetails.append('name', userInfo.user.name);
      googleDetails.append('email', userInfo.user.email);
      googleDetails.append('photo', userInfo.user.photo);
      googleDetails.append('google_signup_id', userInfo.user.id);
      googleDetails.append("todaysDate", todayDate);
      googleDetails.append('time', currenttime);

      if(userInfo.user.email !=''){
        this.setState({spinner: true})
        this.props.googleLogin(googleDetails);
      }
      else{
        Alert.alert("Sorry. Some problem occurs. Please try again in some time.")
      }

    } catch (error) {
      console.log("signin error",error);

      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log("SIGN_IN_CANCELLED-error", error);
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        console.log("IN_PROGRESS-error", error);
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.log("PLAY_SERVICES_NOT_AVAILABLE-error", error);
      } else {
        // some other error happened
        console.log("other-error", error);
      }
    }
  }

  forgetPass = () => {
    this.props.navigation.navigate("ForgotPassword")
  }

  async emailHandler(email) {
    console.log("mark value", this.state.mark);
    this.setState({ facebookButton: 2 });

    if (this.state.mark == 1) {
      const emailAsyncStorage = await AsyncStorage.getItem("loginEmail");
      const passwordAsyncStorage = await AsyncStorage.getItem("loginPassword");

      if (emailAsyncStorage == null) {
        console.log("null");
        this.setState({ email: email });
      } else{
        console.log("checkbox");
        this.setState({
          checkbox: true,
          email: emailAsyncStorage,
          password: passwordAsyncStorage

        });
      }

      this.setState({ mark: 2 });
    } else {
      this.setState({ email: email, password: "" });
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (!reg.test(email)) {
        this.setState({
          email_error: "Enter valid email id"
        });
      } else {
        this.setState({
          email_error: ""
        });
      }
    }
  }

  passwordHandler = password => {
    this.setState({
      password: password,
      password_error: ""
    });
  };

  checkboxHandler = () => {
    if (this.state.email == "") {
      this.setState({ email_error: "Enter email id", facebookButton: 2});
    }
    else if (this.state.checkbox == true) {
      this.setState({ checkbox: false });
    } else {
      this.setState({ checkbox: true });
    }
  };

  handleSubmit = () => {
    if (this.state.email === "") {
      this.setState({
        email_error: "Enter email id"
      });
    }
    if (this.state.password === "") {
      this.setState({
        password_error: "Enter password"
      });
    } else {

      if (this.state.checkbox == false) {
        AsyncStorage.removeItem("loginEmail");
        AsyncStorage.removeItem("loginPassword");
      } else {
        AsyncStorage.setItem("loginEmail", this.state.email);
        AsyncStorage.setItem("loginPassword", this.state.password);
      }

      this.setState({ spinner: true });
      let userDetails = new FormData();
      userDetails.append("email", this.state.email);
      userDetails.append("password", this.state.password);
      this.props.login(userDetails);
    }
  };

  componentDidUpdate(prevProps) {
    if (prevProps.userDetails !== this.props.userDetails) {
      if (this.props.error && this.props.error !== undefined) {
        if (this.state.visible1 === false) {
          this.setState({
            visible1: true,
            popupMsg: "Something went wrong,please try again"
          });
        }
        this.setState({ spinner: false });
      } else {
        if (this.props.userDetails && this.props.userDetails !== undefined && Object.keys(this.props.userDetails).length ) {
          console.log("didurdate",this.props);
          this.setState({ spinner: false });
          if (this.props.userDetails.ack === 1) {
            this.setState({ spinner: false });
            if (this.props.userDetails.userdetail.user_type == "C") {
              console.log("this.props.userDetails",this.props.userDetails);
              this.storedata();
            } else {
              this.setState({
                visible1: true,
                popupMsg:
                  "Cannot login successfully as you are not a register customer"
              });
            }
          } else {
            this.setState({ spinner: false });
            if (this.state.visible1 === false) {
              this.setState({
                visible1: true,
                popupMsg: this.props.userDetails.msg
              });
            }
          }
        } else if (this.props.fbLogin && this.props.fbLogin !== undefined) {
          console.log("this.props.fbLogin---------", this.props.fbLogin);
        } else {
          this.setState({ spinner: false });

          this.setState({
            visible1: true,
            popupMsg: "Not an valid customer"
          });
        }
      }
    }
  }

  async storedata() {
    try {
      console.log("testid1", this.state.login_userid);

      const value = await AsyncStorage.getItem("login_id");
      console.log("loginId------", this.props.userDetails.userId);
      await AsyncStorage.setItem(
        "login_id_2",
        String(this.props.userDetails.userId)
      );
      this.props.navigation.navigate("index");
    } catch (error) {
      console.log(error);
      // Error saving data
    }
  }


    componentDidMount() {
    console.log(this.props);
    this.props.navigation.addListener("willFocus", () => {
      console.log("focus");
      //this.remember();
      console.log("emailAsyncStorage", this.state.email);
      console.log("emailAsyncStorage", this.state.password);
    });
  }

  onAppleButtonPress = async () => {
    console.log('LOGGING')
    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: AppleAuthRequestOperation.LOGIN,
      requestedScopes: [AppleAuthRequestScope.EMAIL, AppleAuthRequestScope.FULL_NAME],
    });
    let apple_userid = appleAuthRequestResponse.user;
    
    // let apple_username = appleAuthRequestResponse.fullName.familyName == null ? "" : appleAuthRequestResponse.fullName.familyName;
    // let apple_email = appleAuthRequestResponse.email == null ? "" : appleAuthRequestResponse.email;
    // let first_name;
    // let last_name;

    //Alert.alert(apple_userid);

    // if(apple_username.includes(" ")){
    //   first_name = apple_username.split(" ")[0];
    //   last_name = apple_username.split(" ")[1];
    // }
    // else{
    //   first_name = naapple_usernameme;
    // }

    let currenttime = moment(new Date()).format("hh:mm:ss");
    let todayDate = moment(new Date()).format("YYYY-MM-DD");

    let appleDetails = new FormData();
    // appleDetails.append('first_name', first_name);
    // appleDetails.append('last_name', last_name);
    // appleDetails.append('name', apple_username);
    // appleDetails.append('email', apple_email ? apple_email : "");
    appleDetails.append('apple_uid', apple_userid);
    appleDetails.append("todaysDate", todayDate);
    appleDetails.append('time', currenttime);
    
    this.setState({spinner: true})
    this.props.appleLogin(appleDetails);
  }


  //  onAppleButtonPress = async() => {
  //   try {
  //     // performs login request
  //     const appleAuthRequestResponse = await appleAuth.performRequest();
      

  //     console.log(appleAuthRequestResponse,"appleAuthRequestResponse");
      
  //     // if (appleAuthRequestResponse['realUserStatus']) {
  //     //   this.setState({
  //     //     isLogin: true,
  //     //   });
  //     // }
  //   } catch (error) {
  //     console.log("error111111",error);
      
  //     // if (error.code === AppleAuthError.CANCELED) {
  //     // }
  //     // if (error.code === AppleAuthError.FAILED) {
  //     //   alert('Touch ID wrong');
  //     // }
  //     // if (error.code === AppleAuthError.INVALID_RESPONSE) {
  //     //   alert('Touch ID wrong');
  //     // }
  //     // if (error.code === AppleAuthError.NOT_HANDLED) {
  //     // }
  //     // if (error.code === AppleAuthError.UNKNOWN) {
  //     //   alert('Touch ID wrong');
  //     // }
  //   }
  // }

  render() {
    console.log("userdetails login render", this.props);

    return (
      <SafeAreaView>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <ImageBackground
          source={require("../../assets/images/mainbg.jpg")}
          style={styles.bodymain}
        >
          <KeyboardAvoidingView enabled behavior="padding">
            <ScrollView>
              <View style={[styles.container, { paddingTop: 16 }]}>
                <Spinner
                  visible={this.state.spinner}
                  textContent={"Loading..."}
                  textStyle={styles.spinnerTextStyle}
                />
                <TouchableOpacity style={styles.logo}>
                  <Image
                    source={require("../../assets/images/logo.png")}
                    resizeMode="contain"
                    style={{ width: 280, height: 80 }}
                  />
                </TouchableOpacity>

                <View style={[styles.mainform, { marginTop: 26 }]}>
                  <Text style={styles.heading}>
                    Login for quick reservations
                  </Text>

                  <TextInput
                    type="email"
                    style={styles.forminput}
                    placeholder="Email"
                    placeholderTextColor="#9590a9"
                    keyboardtype="email"
                    value={this.state.email}
                    onChangeText={email => this.emailHandler(email)}
                  />
                  <Text style={styles.error}>{this.state.email_error}</Text>
                  <TextInput
                    style={styles.forminput}
                    placeholder="Password"
                    placeholderTextColor="#9590a9"
                    value={this.state.password}
                    onChangeText={password => this.passwordHandler(password)}
                    secureTextEntry={true}
                  />
                  {/* <TextInput
                    style={[styles.forminput,{marginBottom: -32}]}
                    placeholder="Password"
                    placeholderTextColor="#9590a9"
                    value={this.state.password}
                    onChangeText={password => this.passwordHandler(password)}
                    secureTextEntry={true}
                  /> */}
                  <Text style={styles.error}>{this.state.password_error}</Text>
                  <View
                    style={{
                      flexDirection: "column",
                      // justifyContent: "flex-start",
                      // alignItems: "center",
                     // paddingLeft: 10,
                      paddingBottom: 16
                    }}
                  >
                  <View style={{ flexDirection: "row" }}>
                      <CheckBox
                        style={{ color: "black" }}
                        checked={this.state.checkbox}
                        onPress={this.checkboxHandler}
                        checkedColor="#e25cff"
                      />
                      <Text
                        style={{
                          paddingTop: 15,
                          color: "#e25cff",
                          fontSize: 18,
                          right: 20
                        }}
                      >
                        Remember me
                      </Text>
                    </View>
                    <TouchableOpacity
                    style={{width: '100%', justifyContent:'center', alignItems: 'center', paddingTop: 20}}
                      onPress={this.forgetPass}
                    >
                      <Text
                        style={{
                          color: "#e25cff",
                          fontSize: 18,
                        }}
                      >
                        Forgot password ?{" "}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  {/* <TouchableOpacity
                    style={styles.forgetpass}
                    onPress={() =>
                      this.props.navigation.navigate("ForgotPassword")
                    }
                  >
                    <Text style={styles.pinktext}>Forgot Password</Text>
                  </TouchableOpacity> */}
                  <TouchableOpacity
                    style={styles.pinkbtn}
                    onPress={() => this.handleSubmit()}
                  >
                    <Text style={styles.btntext}>SIGN IN</Text>
                  </TouchableOpacity>

                  <Image
                    source={require("../../assets/images/or.png")}
                    resizeMode="contain"
                    style={styles.orline}
                  />

                  <View style={styles.container}>
                    <Dialog
                      visible={this.state.visible1}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      onTouchOutside={() => {
                        this.setState({ visible1: false });
                      }}
                      dialogStyle={{ width: "80%" }}
                      footer={
                        <DialogFooter>
                          <DialogButton
                            textStyle={{
                              fontSize: 14,
                              color: "#333",
                              fontWeight: "700"
                            }}
                            text="OK"
                            onPress={() => {
                              this.setState({ visible1: false });
                            }}
                          />
                        </DialogFooter>
                      }
                    >
                      <DialogContent>
                        <Text style={styles.popupText}>
                          {this.state.popupMsg}
                        </Text>
                      </DialogContent>
                    </Dialog>
                  </View>

                  <View style={[styles.jsbtn, { marginTop: -20}]}>
                    {/* <TouchableOpacity style={styles.googlebtn}>
                      <Text style={styles.btntext}>
                        <Icon name="google" style={{ marginRight: 10 }} /> With
                        GOOGLE
                      </Text>
                    </TouchableOpacity> */}
                    <GoogleSigninButton
                    style={{ width: 136, height: 52.5 }}
                    //size={GoogleSigninButton.Size.Wide}
                    color={GoogleSigninButton.Color.Dark}
                    onPress={this.signIn}
                   // disabled={this.state.isSigninInProgress}
                   />

                      <TouchableOpacity style={styles.googlebtn} onPress={this.faceBookLogin}>
                      <Text style={styles.btntext}>
                        <Icon name="facebook" style={{ marginRight: 10 }} />
                        {"  "}FACEBOOK
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.appleView}>
                  <View style={styles.applebtn}>
                    <AppleButton
                        buttonStyle={AppleButton.Style.WHITE}
                        buttonType={AppleButton.Type.SIGN_IN}
                        style={{
                          width: 160,
                          height: 45,
                        }}
                        onPress={() => this.onAppleButtonPress()}
                    />
                  </View>
                  </View>
                  <View style={[styles.bttext, { marginBottom: 40 }]}>
                    <Text style={styles.text}>New to Spa Now? </Text>
                    <TouchableOpacity
                      onPress={() => this.props.navigation.navigate("Signup")}
                    >
                      <Text style={styles.pinktext}>Register Now</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.auth
});

export default connect(mapStateToProps, { login, fbLogin, googleLogin, appleLogin })(LoginScreen);
