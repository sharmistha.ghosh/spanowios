import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  AsyncStorage,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  Alert
} from "react-native";
import _ from "lodash";
import { Icon } from "react-native-elements";
import styles from "./style";
import { setSelectedSpa, spaDetailsList } from "./redux/spaAction";
import { connect } from "react-redux";
import moment from "moment";
//import {getCurrentPosition} from 'react-native-geolocation-service';
import Axios from "axios";
import Geocoder from "react-native-geocoding";
import Spinner from "react-native-loading-spinner-overlay";
import MapView, { Marker } from "react-native-maps";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import { add } from "react-native-reanimated";

// Required For Geo Location
//import Geolocation from '@react-native-community/geolocation';

class index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      mapview: [],
      login_userid: "",
      latitudeformmap: 0,
      longitudefrommap: 0,
      // latitude: 22.5944516,
      // longitude: 88.3835325,
      location: {},
      visible1: false,
      popupMsg: "",
      spinner: true
    };

    todayDate = moment(new Date()).format("YYYY-MM-DD");
    this.locationName();
    //this.latLongdata();
    this.spaList();
  }

  //--FETCHING SPA LISTING USING REDUX--//
  async spaList() {
    console.log("fetch");

    //FETCH CUSTOMER ID FROM ASYNCSTORAGE//
    try {
      const value = await AsyncStorage.getItem("login_id_2");
      console.log("loginId-----------------", value);
      if (value) {
        this.setState({ login_userid: value });
      } else {
        this.props.navigation.navigate("Login");
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
  }

  componentDidMount() {
    this.props.navigation.addListener("willFocus", () => {
      this.setState({ spinner: true });
      console.log("focus");
      this.locationName();
    });
  }

  locationName = () => {
    //current location fetch
    console.log("fetching lat lon");

    Axios.get("http://ip-api.com/json")
      .then(location => {
        console.log("location====", location.data);

        let location1= {
          latitude: location.data.lat,
          longitude: location.data.lon,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421
        }

        // let location2 = {
        //   latitude: 34.0273047,
        //   longitude: -118.4652258,
        //   latitudeDelta: 0.0922,
        //   longitudeDelta: 0.0421
        // };

        this.setState({ location: location1 });

       // console.log("new date",new Date());
       // let todayDate = moment(new Date()).format("YYYY-MM-DD");

       // Alert.alert(todayDate)
        

        let userDetails = new FormData();
        userDetails.append("todaysDate", todayDate);
          userDetails.append("currentLattitude", location.data.lat);
          userDetails.append("currentLongitude", location.data.lon);

       // userDetails.append("currentLattitude", "34.0273047");
      // userDetails.append("currentLongitude", "-118.4652258");

        console.log("discount------", userDetails);
        this.props.spaDetailsList(userDetails);
      })
      .catch(err => {});
  };

  // // Loction Picker
  // locationpicker() {
  //   console.log('fetch location');

  //   Geolocation.getCurrentPosition(info => {
  //     this.setState({latitudeformmap: info['coords']['latitude']});
  //     this.setState({longitudefrommap: info['coords']['longitude']});
  //     Alert.alert(
  //       this.state.latitudeformmap + ' ' + this.state.longitudefrommap,
  //     );
  //   });

  //   // let geolocationdata = new FormData();
  //   // geolocationdata.append('latitudeformmap', this.state.latitudeformmap);
  //   // geolocationdata.append('longitudefrommap', this.state.longitudefrommap);

  //   // // Calling API
  //   // Api.postApi('userImageUpload', geolocationdata).then((georesdata) => {
  //   //   alert("Your Location Saved Successfully !!")
  //   //   }
  //   //   ).catch((err) => {
  //   //     alert("Error from Geo Location !! ");
  //   //   })
  // }

  // //Lat-long fetch
  // latLongdata = () => {
  //   Alert.alert("longitude");
  //   console.log("longitude");

  //   // let getPosition = function(options) {
  //   //   return new Promise(function(resolve, reject) {
  //   //     navigator.geolocation.getCurrentPosition(resolve, reject, options);
  //   //   });
  //   // };
  //   // getPosition().then(position => {
  //   //   console.log("position",position);

  //   //   if (position) {
  //   //     this.setState({
  //   //       latitude: position.coords.latitude,
  //   //       longitude: position.coords.longitude
  //   //     });
  //   //   }
  //   // });
  //   console.log("calculate lat long");

  //   Geocoder.init("AIzaSyBvdYqm0D5dLlFgckXoouXPtqNFlxvMfwU");
  //   Geocoder.from("new delhi")
  //     .then(json => {
  //       console.log(json);
  //       console.log("calculate lat long");

  //       var location = json.results[0].geometry.location;
  //       console.log("lat long", location);
  //     })
  //     .catch(error => console.warn("error", error));
  // };

  //----STORING SPADETAILS USING REDUX----//
  async storedata(spaId, spaName, spaAddress, spaDiscount) {
    if (this.state.visible1 == true) {
      this.setState({ visible1: false });
    }
    this.props.setSelectedSpa({ spaId, spaName, spaAddress, spaDiscount });
    this.props.navigation.navigate("details");
  }

  componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps", nextProps);
    this.setState({ spinner: false });
    var mapview1 = [];

    if (nextProps.spaDiscount && nextProps.spaDiscount.spa_list) {
      for (var i = 0; i < nextProps.spaDiscount.spa_list.length; i++) {
        mapview1.push({
          latitude: Number(nextProps.spaDiscount.spa_list[i].lattitude),
          longitude: Number(nextProps.spaDiscount.spa_list[i].longitude),
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
          spaName: nextProps.spaDiscount.spa_list[i].spa_name,
          id: nextProps.spaDiscount.spa_list[i].id,
          address: nextProps.spaDiscount.spa_list[i].address,
          discount: nextProps.spaDiscount.spa_list[i].discount
        });
      }
    }
    this.setState({ mapview: mapview1 });
    console.log("gggggggggggggggggggg", mapview1);

    // this.setState({

    //   location: {
    //     latitude: Number(nextProps.spaDetails.spa_details.lattitude),
    //     longitude: Number(nextProps.spaDetails.spa_details.longitude),
    //     latitudeDelta: 0.0922,
    //     longitudeDelta: 0.0421,
    //   },
    // });
    //this.setState(nextProps);
  }

  markerClick = (id, spaName, address, discount) => {
    console.log("markerClick");
    if (this.state.visible1 === false) {
      this.setState({
        visible1: true,
        popupMsg: spaName,
        id: id,
        spaName: spaName,
        address: address,
        discount: discount
      });
    }
  };

  closePopupbox = () => {
    this.setState({
      visible1: false
    });
    console.log("closePopupbox");
  };

  render() {
    // console.log("props in index screen", this.props);
    // console.log(
    //   "props in index screen location",
    //   Object.keys(this.state.location).length
    // );
    let locationmap = Object.keys(this.state.location).length;

    let datetime = new Date();
    let date = moment(new Date()).format("YYYY-MM-DD");
    let time = moment(new Date()).format("hh-mm");
    console.log("date in render",date,time);
    
    // if (this.props.spaDiscount && this.props.spaDiscount.length) {
    //   var this.props.spaDiscount.spa_list = this.props.spaDiscount.spa_list;
    // }

    //   console.log('props in render', this.props);
    //   // if(this.props.spaDiscount.length
    //   // ){
    //     console.log("count-----------------------",Object.keys(this.props.spaDiscount.spa_list).length);
    //  // }

    // console.log('this.props.spaDiscount.spa_list in render', this.props.spaDiscount);
    // var count = Object.keys(this.props.spaDiscount.spa_list).length;
    // console.log("count",count);
    // if(this.props.spaDiscount.length){

    // }
    // console.log("RENDER", Object.keys(this.props.spaDiscount.spa_list).length);

    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View
          style={[styles.topbar, { paddingTop: 10.5, paddingBottom: 10.5 }]}
        >
          <Image
            source={require("../../assets/images/logo.png")}
            resizeMode="contain"
            style={{ width: 200, height: 60 }}
          />
          {/* <TouchableOpacity
            style={styles.notify}
           // onPress={this.latLongdata.bind(this)}
            onPress={() => this.props.navigation.navigate('CustomCardScreen')}
          >
            <Image
              source={require('../../assets/images/bell.png')}
              resizeMode="contain"
              style={{width: 40, height: 35}}
            />
          </TouchableOpacity> */}
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={styles.spinnerTextStyle}
        />
        <ScrollView>
          {this.props.spaDiscount.spa_list &&
          this.props.spaDiscount.spa_list.length ? (
            <View>
              <View style={styles.maparea}>
                {locationmap == 0 ? null : (
                  <MapView
                    style={{ width: "100%", height: 300 }}
                    initialRegion={this.state.location}
                  >
                    {this.state.mapview.map(marker => (
                      <Marker
                        onPress={() =>
                          this.markerClick(
                            marker.id,
                            marker.spaName,
                            marker.address,
                            marker.discount
                          )
                        }
                        coordinate={{
                          latitude: marker.latitude,
                          longitude: marker.longitude,
                          latitudeDelta: 0.0922,
                          longitudeDelta: 0.0421
                        }}
                      ></Marker>
                    ))}
                    <Dialog
                      visible={this.state.visible1}
                      onTouchOutside={() => {
                        this.closePopupbox();
                      }}
                      dialogStyle={{ width: "50%", marginTop: -300 }}
                    >
                      <DialogContent>
                        <TouchableOpacity
                          onPress={() =>
                            this.storedata(
                              this.state.id,
                              this.state.spaName,
                              this.state.address,
                              this.state.discount
                            )
                          }
                        >
                          <Text
                            style={[
                              styles.popupText,
                              { color: "#800080", fontWeight: "bold" }
                            ]}
                          >
                            {this.state.popupMsg}
                          </Text>
                        </TouchableOpacity>
                      </DialogContent>
                    </Dialog>
                  </MapView>
                )}
              </View>

              <View style={[styles.container, styles.mtop]}>

                {this.props.spaDiscount.spa_list.map((key, values) => (
                  <TouchableOpacity
                    key={key}
                    style={styles.cardhome}
                    onPress={() =>
                      this.storedata(
                        key.id,
                        key.spa_name,
                        key.address,
                        key.discount
                      )
                    }
                  >
                    {key.image ? (
                      <ImageBackground
                        source={{ uri: `${key.image}` }}
                        style={styles.cradimage}
                      >
                        <View style={styles.cardimageview}>
                          <View>
                            <Text style={styles.imageheading}>
                              {key.spa_name}
                            </Text>
                            <View>
                              <View style={styles.place}>
                                <Image
                                  source={require("../../assets/images/marker.png")}
                                  resizeMode="contain"
                                  style={{
                                    width: 24,
                                    height: 30,
                                    marginRight: 10
                                  }}
                                />
                                <Text style={styles.placetext}>
                                  {key.address}
                                </Text>
                              </View>
                            </View>
                          </View>
                        </View>
                      </ImageBackground>
                    ) : (
                      <ImageBackground
                        source={require("../../assets/images/card1.jpg")}
                        style={styles.cradimage}
                      >
                        <View style={styles.cardimageview}>
                          <View>
                            <Text style={styles.imageheading}>
                              {key.spa_name}
                            </Text>
                            <View>
                              <View style={styles.place}>
                                <Image
                                  source={require("../../assets/images/marker.png")}
                                  resizeMode="contain"
                                  style={{
                                    width: 24,
                                    height: 30,
                                    marginRight: 10
                                  }}
                                />
                                <Text style={styles.placetext}>
                                  {key.address}
                                </Text>
                              </View>
                            </View>
                          </View>
                        </View>
                      </ImageBackground>
                    )}

                    <View style={styles.cardbody}>
                      {key.discount && key.discount > 0 ? (
                        <View style={styles.offermain}>
                          <Text style={styles.offertext}>
                            Deal {key.discount}% Off
                          </Text>
                        </View>
                      ) : null}

                      <View>
                        <View style={[styles.justifyrow, { marginBottom: 1 }]}>
                          <Text style={styles.mutetext}>Distance:</Text>
                          <Text style={styles.whitetext}>
                            {" "}
                            {key.distance.toFixed(1)} Miles
                          </Text>
                        </View>
                        <View
                          style={[
                            styles.starwrap,
                            { justifyContent: "center", marginTop: 4 }
                          ]}
                        >
                          {_.times(key.rating, i => (
                            <Icon name="star" color="#e25cff" key={i} />
                          ))}

                          {_.times(5 - key.rating, i => (
                            <Icon name="star" color="#5a5a71" key={i} />
                          ))}
                          <View style={{ width: 110, marginLeft: 80 }}>
                            <View
                              style={[styles.justifyrow, { flexWrap: "wrap" }]}
                            >
                              <Text style={styles.mutetext}>Price:</Text>
                              <Text style={styles.whitetext}> 1hr</Text>
                              <Text style={styles.whitetext}>
                                $ {key.basic_service_price_per_hour}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                ))}
                {/* End of card */}
              </View>
            </View>
          ) : null}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.spa
});
export default connect(mapStateToProps, {
  setSelectedSpa,
  spaDetailsList
})(index);
