import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './style';
class Service extends Component {
  render() {
    console.log('service', service);

    const service = this.props.service;
    return (
      <View>
        {service && service.length ? (
          <View>
          <Text style={styles.h1}> Available Services</Text>
          {service.map((item, i) => (
            <View style={styles.graycard} key={i}>
              <Text style={styles.h2}>{item.service_name}</Text>
              {item.price_duration.map((item1, j) => (
                <View style={styles.flexwrap}>
                  <View style={styles.justifyrow}>
                    <Image
                      source={require('../../assets/images/clock.png')}
                      resizeMode="contain"
                      style={{width: 14, height: 14, marginRight: 8}}
                    />
                    <Text style={styles.mutetext2}>
                      $ {item1.price} / {item1.duration} min
                    </Text>
                  </View>
                </View>
              ))}
            </View>
          ))}
        </View>
        ) : 
        null}
      </View>
    );
  }
}

export default Service;
