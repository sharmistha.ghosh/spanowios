import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  AsyncStorage,
  Alert,
  TextInput
} from "react-native";
import { Icon, CheckBox } from "react-native-elements";
import styles from "./style";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import { connect } from "react-redux";
import { finalBooking, setSelectedSpa } from "./redux/spaAction";

import Spinner from "react-native-loading-spinner-overlay";

class booking extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      customer_id: "",
      spaid: "",
      service_id: "",
      individual_price: "",
      booking_date: "",
      booking_time: "",
      service_duration: "",
      service_name: "",
      spa_name: "",
      spa_address: "",
      totalduration: "",
      price: "",
      checkedTips: false,
      tips: "",
      final_amount_with_tips: "",
      visible1: false,
      popupMsg: "",
      spa_discount: "",
      final_amount_without_tips: "",
      spinner: false,
      adminPerCent : this.props.navigation.state.params ? this.props.navigation.state.params.adminBookingFees : "",
      specialOfferAvailable : this.props.navigation.state.params ? this.props.navigation.state.params.specialOfferAvailable : false,
      specialDiscountPercent : this.props.navigation.state.params ? this.props.navigation.state.params.specialDiscountPercent : "",
     // adminPerCent : 10,
      adminBookingFee : "",
      timeerror: ""
    };
    this.setAsyncStorageData();
  }

  //--FETCHING SERVICE DETAILS, SPA DETAILS USING REDUX--//
  setAsyncStorageData = async () => {
    console.log(this.props.selectedServiceDetails.date,"this.props.selectedServiceDetails.date");
    
    try {
      const login_id = await AsyncStorage.getItem("login_id_2");

      if (login_id) {
        this.setState({ customer_id: login_id });
      }
    } catch (error) {
      // Error retrieving data
    }
    this.setState({
      spaid: this.props.selectedSpaDetails.spaId,
      spa_name: this.props.selectedSpaDetails.spaName,
      spa_address: this.props.selectedSpaDetails.spaAddress
      // spa_discount: this.props.selectedSpaDetails.spaDiscount,
    });

    var price = this.props.selectedServiceDetails.totalPrice;
    price = price.toFixed(2);

    this.setState({
      service_id: this.props.selectedServiceDetails.serviceIds,
      individual_price: this.props.selectedServiceDetails.servicePrices,
      booking_date: this.props.selectedServiceDetails.date,
      booking_time: this.props.selectedServiceDetails.time,
      service_duration: this.props.selectedServiceDetails.serviceDurations,
      price: price,
      service_name: this.props.selectedServiceDetails.serviceNames,
      totalduration: this.props.selectedServiceDetails.totalduration
    });

    var finalDiscount;

    if(this.state.specialOfferAvailable){
      finalDiscount= this.state.specialDiscountPercent
    }
    else{
      finalDiscount= this.props.selectedSpaDetails.spaDiscount
    }

    if (finalDiscount != "") {
      this.setState({
        spa_discount: finalDiscount
      });
      var dis = finalDiscount;
    } else {
      this.setState({ spa_discount: 0 });
      var dis = 0;
    }


    console.log("admin fees",this.state.adminPerCent);
    
    var feespercent = "";
    var bookingfees = 0;
    var discount = (this.props.selectedServiceDetails.totalPrice * dis) / 100;
    if(this.state.adminPerCent != ""){
    feespercent = this.state.adminPerCent;
    bookingfees = (this.props.selectedServiceDetails.totalPrice * feespercent) / 100;
    }
    this.setState({ adminBookingFee : bookingfees })
    console.log("discount", discount);
    console.log("bookingfees", bookingfees);
    var sub_totals = this.props.selectedServiceDetails.totalPrice - discount + bookingfees;
    console.log("sub_total", sub_totals);


    sub_totals = sub_totals.toFixed(2);

    this.setState({
      final_amount_without_tips: sub_totals,
      final_amount_with_tips: sub_totals
    });
  };

  //--SELECT TIPS AND CALCULATING THE TOTAL PRICE--//
  tipsHandler = tips => {
    console.log("tips",tips);
    console.log("this.state.final_amount_with_tips",this.state.final_amount_with_tips);
    let priceTips = 0;
    if (tips) {
      console.log("tips");
      
      priceTips +=
        parseFloat(this.state.final_amount_with_tips) + parseFloat(tips);

      priceTips = priceTips.toFixed(2);
      this.setState({
        tips: tips,
        final_amount_without_tips: priceTips
      });
    } else {
      console.log(" no tips");

     let priceTips3 = this.state.final_amount_with_tips;
     console.log("priceTips3",priceTips3);
     
    // let priceTips4 = priceTips3.toFixed(2);

      this.setState({
        tips: tips,
        final_amount_without_tips: priceTips3
      });
    }
  //  console.log("totalprice", priceTips);
  };

  //--CLOSE POPUP BOX--//
  closePopupbox = () => {

    if (this.state.visible1 == true) {
      this.setState({ visible1: false });
    }
  };

  //----SUBMIT FINAL BOOKING DETAILS USING REDUX----//
  handleSubmit = () => {
    // this.setState({ spinner: true });
    // let bookingDetails = new FormData();
    // bookingDetails.append("customer_id", this.state.customer_id);
    // bookingDetails.append(
    //   "masseuse_id",
    //   this.props.navigation.state.params.send_masseuseId
    //     ? this.props.navigation.state.params.send_masseuseId
    //     : ""
    // );
    // console.log("all state before sending",this.state);
    
    // bookingDetails.append("spa_id", this.state.spaid);
    // bookingDetails.append("booking_date", this.state.booking_date);
    // bookingDetails.append("booking_start_time", this.state.booking_time);
    // bookingDetails.append("arr_service_ids", this.state.service_id);
    // bookingDetails.append("arr_service_names", this.state.service_name);
    // bookingDetails.append("arr_service_durations", this.state.service_duration);
    // bookingDetails.append("arr_service_prices", this.state.individual_price);

    // bookingDetails.append("service_tips", this.state.tips);
    // bookingDetails.append("discount", this.state.spa_discount);
    // bookingDetails.append("admin_payment_amount", this.state.adminBookingFee);
    // bookingDetails.append("final_amount", this.state.final_amount_without_tips);
    

    this.props.navigation.navigate("CardFormScreen", {
      customer_id: this.state.customer_id,
      masseuse_id: this.props.navigation.state.params.send_masseuseId ? this.props.navigation.state.params.send_masseuseId : "",
      spa_id : this.state.spaid,
      booking_date : this.state.booking_date,
      booking_start_time : this.state.booking_time,
      arr_service_ids : this.state.service_id,
      arr_service_names : this.state.service_name,
      arr_service_durations : this.state.service_duration,
      arr_service_prices : this.state.individual_price,
      service_tips : this.state.tips,
      discount : this.state.spa_discount,
      admin_payment_amount : this.state.adminBookingFee,
      final_amount : this.state.final_amount_without_tips,
      adminBookingFees : this.state.adminPerCent
    });

   // console.log("bookingDetails", bookingDetails);
   // this.props.finalBooking(bookingDetails);
  };

  componentDidUpdate(prevProps) {

    if (prevProps !== this.props) {
      this.setState({ spinner: false });
      const addDetails = this.props.finalBookingResponse;
      if (this.props.error && this.props.error !== undefined) {

        // if (this.state.visible1 === false) {
        //   this.setState({visible1: true, popupMsg: addDetails.msg});
        // }

      //  Alert.alert("Booking Not Successfull. Something went wrong.");
      } else {

        if (addDetails.Ack === 1) {
          this.props.navigation.navigate("CardFormScreen", {
            booking_id: addDetails.result.booking_id
          });
        } else {
          // this.setState({
          //   spinner: false,
          // });
          // if (this.state.visible1 === false) {
          //   this.setState({
          //     visible1: true,
          //     popupMsg: "Booking Not Successfull. Something went wrong."
          //   });
          // }
          //Alert.alert("Booking Not Successfull. Something went wrong.");
        }
      }
    }
  }

  render() {
    console.log("props value in render booking page", this.props);

    const { goBack } = this.props.navigation;
    const masseuseId = this.props.navigation.state.params.send_masseuseId;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Booking Confirmation</Text>
          </View>
          {/* <TouchableOpacity style={styles.notify}>
            <Image
              source={require('../../assets/images/bell.png')}
              resizeMode="contain"
              style={{width: 40, height: 35}}
            />
          </TouchableOpacity> */}
        </View>
        <ScrollView>
          <View style={styles.graycontainer}>
            <View style={[styles.graycard,{paddingRight: 16}]}>
              <Text style={[styles.h3, { marginBottom: 15 }]}>
                {" "}
                {this.state.spa_name}{" "}
              </Text>
              <View style={styles.justifyrow}>
                <Image
                  source={require("../../assets/images/marker-gray.png")}
                  resizeMode="contain"
                  style={{ width: 20, height: 25, marginRight: 10 }}
                />
                <Text style={styles.mutetext}>{this.state.spa_address}</Text>
              </View>
              <View style={styles.justifyrow}>
                <Text style={styles.mutetext}>Date:</Text>
                <Text style={styles.whitetext}>{this.state.booking_date}</Text>
              </View>
              <View style={styles.justifyrow}>
                <Text style={styles.mutetext}>Time:</Text>
                <Text style={styles.whitetext}>{this.state.booking_time}</Text>
              </View>
              <View style={styles.justifyrow}>
                <Text style={styles.mutetext}>{this.state.service_name} </Text>
                <Text style={styles.whitetext}></Text>
              </View>
              <View style={styles.justifyrow}>
                <Text style={styles.mutetext}>Duration:</Text>
                <Text style={styles.whitetext}>{this.state.totalduration}</Text>
              </View>
              <View style={styles.justifyrow}>
                <Text style={styles.mutetext}>Masseuse Name:</Text>
                {masseuseId ? (
                  <Text style={styles.whitetext}>
                    {this.props.navigation.state.params.send_masseuseName}
                  </Text>
                ) : (
                  <Text style={styles.whitetext}>No Choice</Text>
                )}
              </View>
              <View
                style={
                  this.state.checkedTips ? styles.whitecard : styles.graycard
                }
              >
                <CheckBox
                  title="Add tip now (Optional)"
                  uncheckedIcon="check-square"
                  checkedIcon="check-square"
                  containerStyle={styles.chkcontainer}
                  textStyle={[styles.txtstyle]}
                  checkedColor="#e25cff"
                  uncheckedColor="#4e4e5b"
                  onPress={() =>
                    this.setState({ checkedTips: !this.state.checkedTips })
                  }
                />
              </View>
              {this.state.checkedTips ? (
                <TextInput
                  style={styles.forminput}
                  placeholder="Enter Tip Amount"
                  placeholderTextColor="#9590a9"
                  onChangeText={tips => this.tipsHandler(tips)}
                />
              ) : (
                <Text style={styles.whitetext}></Text>
              )}
            </View>
            <View style={styles.totalcount}>
              <Text style={[styles.h1, { marginBottom: 0 }]}>
                Total Price:{" "}
              </Text>
              <Text style={styles.pinktext}>${this.state.price}</Text>
            </View>
            <View>
              <View style={styles.totalcount}>
                <Text style={[styles.h1, { marginBottom: 0 }]}>Discount: </Text>
                <Text style={styles.pinktext}>{this.state.spa_discount}%</Text>
              </View>
              <View style={styles.totalcount}>
                <Text style={[styles.h1, { marginBottom: 0 }]}>Booking Fee: </Text>
                <Text style={styles.pinktext}>{this.state.adminPerCent}%</Text>
              </View>
              <View style={styles.totalcount}>
                <Text style={styles.h1}>
                  Sub Totals:{" "}
                </Text>
                <Text style={styles.pinktext}>
                  ${this.state.final_amount_without_tips}
                </Text>
              </View>
            </View>
            <TouchableOpacity
              style={styles.pinkbtn}
              onPress={() => this.handleSubmit()}
            >
              <Text style={styles.btntext}>Pay and Relax</Text>
            </TouchableOpacity>
          </View>
          <Spinner
            visible={this.state.spinner}
            textContent={"Loading..."}
            textStyle={styles.spinnerTextStyle}
          />
          <View style={styles.container}>
            <Dialog
              visible={this.state.visible1}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              onTouchOutside={() => {
                this.closePopupbox();
              }}
              dialogStyle={{ width: "80%" }}
              footer={
                <DialogFooter>
                  <DialogButton
                    textStyle={{
                      fontSize: 14,
                      color: "#333",
                      fontWeight: "700"
                    }}
                    text="OK"
                    onPress={() => {
                      this.closePopupbox();
                    }}
                  />
                </DialogFooter>
              }
            >
              <DialogContent>
                <Text style={styles.popupText}>{this.state.popupMsg}</Text>
              </DialogContent>
            </Dialog>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
const mapStateToProps = state => ({
  ...state.spa
});
export default connect(mapStateToProps, {
  finalBooking,
  setSelectedSpa
})(booking);
