import React, {Component} from 'react';
import {
    Text,
    View,
    Image,
  } from 'react-native';
  import styles from './style';
  import {Avatar} from 'react-native-elements';
  import moment from 'moment';
  import _ from 'lodash';
  import {Icon} from 'react-native-elements';

class Review extends Component {

  constructor(props) {
    super(props);
    this.state = {
     currentDate: new Date(),
    }; 
  }
    //----return date difference----//  
    renderDate  = (date) => {
      let d1 = moment(date[0]);
      let d2 = moment(moment().format("YYYY-MM-DD"));
      return d2.diff(d1, 'days');
  }

  render() {
    const review = this.props.review;
    const spaRating = this.props.spaRating;

    console.log("review", review);
    
    

    return (
      <View>
        {review && review.length ? (
          <View style={styles.graycard}>
          <Text style={{fontSize: 20, color: '#fff', marginBottom: 0}}>
            Customer Reviews{' '}
          </Text>
          <Text style={styles.rev}>{spaRating} Star Rating</Text>
  
          {review.map((item,i) => (
          
            <View style={styles.media} key={i}>
              <Avatar
                rounded
                source={{uri: `${item.image}`}}
                size={60}
                containerStyle={{
                  borderWidth: 1,
                  borderColor: '#676782',
                  padding: 6,
                }}
              />
              <View style={styles.mediabody}>
                <View style={styles.justifycontent}>
                  <Text style={styles.h3}>{item.name}</Text>
                  {/* <Text style={styles.mutetext}>{this.renderDate(item.created_at.split(" ",1))} days ago</Text> */}
                </View>
                {/* <View
                  style={[styles.place, {justifyContent: 'flex-start'}]}>
                  <Image
                    source={require('../../assets/images/marker.png')}
                    resizeMode="contain"
                    style={{width: 24, height: 30, marginRight: 10}}
                  />
                    <Text style={styles.mutetext}>{item.address}</Text>  
                </View> */}
                  <View style={[
                      styles.starwrap,
                      { marginTop: 10},
                    ]}>
                  {_.times(item.rating, i =>
                    (<Icon name="star" color="#e25cff" key={i} />)
                  )}
  
                  {_.times(5-item.rating, i =>
                    (<Icon name="star" color="#5a5a71" key={i} />)
                  )}    
                  </View>
                  <Text style={styles.para}>{item.comment}</Text>
               
              </View>
            </View>
          ))}
        </View>
        ) : null}
        
      </View>
         
    
     

    );
  }
}

export default Review;

