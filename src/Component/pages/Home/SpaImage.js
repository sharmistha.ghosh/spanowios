import React, {Component} from 'react';
import {
    Text,
    View,
    Image,   
  ImageBackground
  } from 'react-native';
import styles from './style';
import Swiper from 'react-native-swiper';
import _ from 'lodash';
import {Icon} from 'react-native-elements';

class SpaImage extends Component {

  render() {
    return (
        <View style={{height: 350, width: '100%'}}>
          {this.props.image && this.props.image.length ? (
          <Swiper
          autoplay={false}
          showsButtons={false}
          dotColor={'#a9a9d4'}
          activeDotColor={'#e25cff'}
          dotStyle={{width: 15, height: 15, borderRadius: 50}}
          activeDotStyle={{width: 15, height: 15, borderRadius: 50}}>
            
           { this.props.image.map( (item, i) => (
          <ImageBackground source={{uri: `${item}`}} style={styles.slide} key={i}>
            <View style={styles.cardimageview}>
              <View>
                <Text style={styles.imageheading}>
                  {' '}
                  {this.props.spaName}
                </Text>
                {/* <View style={styles.place}>
                  <Image
                    source={require('../../assets/images/marker.png')}
                    resizeMode="contain"
                    style={{width: 24, height: 30, marginRight: 10}}
                  />
                  <Text style={styles.placetext}>
                     {this.props.spaAddress}
                  </Text>
                </View> */}
                <View style={[
                    styles.starwrap,
                    {justifyContent: 'center', marginTop: 10},
                  ]}>
                {_.times(this.props.spaRating, i =>
                  (<Icon name="star" color="#e25cff" key={i} />)
                )}

                {_.times(5-this.props.spaRating, i =>
                  (<Icon name="star" color="#5a5a71" key={i} />)
                )}    
                </View>
              </View>
            </View>
          </ImageBackground>
          ))}   
   
        </Swiper> 
         ) : (
          <ImageBackground source={require('../../assets/images/card3.jpg')} style={styles.slide}>
          <View style={styles.cardimageview}>
          <View>
            <Text style={styles.imageheading}>
              {' '}
              {this.props.spaName}
            </Text>
            {/* <View style={styles.place}>
              <Image
                source={require('../../assets/images/marker.png')}
                resizeMode="contain"
                style={{width: 24, height: 30, marginRight: 10}}
              />
              <Text style={styles.placetext}>
                 {this.props.spaAddress}
              </Text>
            </View> */}
            <View style={[
                styles.starwrap,
                {justifyContent: 'center', marginTop: 10},
              ]}>
            {_.times(this.props.spaRating, i =>
              (<Icon name="star" color="#e25cff" key={i} />)
            )}

            {_.times(5-this.props.spaRating, i =>
              (<Icon name="star" color="#5a5a71" key={i} />)
            )}    
            </View>
          </View>
        </View>
        </ImageBackground>
         ) }   
      </View>

    );
  }
}



export default SpaImage;

