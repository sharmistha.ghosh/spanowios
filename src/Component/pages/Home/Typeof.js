import React, {Component} from 'react';
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import {Icon} from 'react-native-elements';
import styles from './style';
import {connect} from 'react-redux';
import {setSelectedService} from './redux/spaAction';
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter,
} from 'react-native-popup-dialog';
import TypeOfServices from './TypeOfServices';
import TypeOfDateTime from './TypeOfDateTime';

class Typeof extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      checked: '',
      totalprice: 0,
      pressStatus: '0',
      visible: false,
      popupMsg: '',
      bookingArray: {},
      timingArray: {},
      serviceName: '',
      serviceDate: '',
      service: this.props.navigation.state.params.service,
    };
    console.log("prps",this.props.navigation.state.params.service);
    
  }

  //--FETCHING THE SELECTED SERVICES FROM TYPEOFSERVICE COMPONENT--//
  selectService = value => {
    console.log('selected service at parent page', value);
    this.setState({
      bookingArray: value,
      totalprice: value.totalPrice,
      serviceName: value.serviceNames,
    });
    console.log('service list in booking array', this.state.bookingArray);
  };

  //--FETCHING THE SELECTED SERVICE DATE AND TIME FROM TYPEOFDATETIME COMPONENT--//
  selectDateTime = value => {
    console.log('selected date_time at parent page', value);
    this.setState({
      timingArray: value,
      serviceDate: value.date,
    });
    console.log('service list in booking array', this.state.timingArray);
  };

  //--STORING SERVICE DETAILS USING REDUX FOR FUTURE USE--//
  handleSubmit = () => {
    if (this.state.serviceName == '') {
      this.setState({
        visible: true,
        popupMsg: 'Please select service name along with the service duration',
      });
    } 
    else if (this.state.timingArray.time == '' || this.state.timingArray.date == '') {
      this.setState({
        visible: true,
        popupMsg:
          'Please select valid date and time slot',
      });
    } else if (this.state.serviceDate == '') {
      this.setState({
        visible: true,
        popupMsg:
          'Either date or time is missing. Please select both service date and time slot',
      });
    } else {
      let service_details = {
        date: this.state.timingArray.date,
        time: this.state.timingArray.time,
        serviceIds: this.state.bookingArray.serviceIds,
        serviceNames: this.state.bookingArray.serviceNames,
        serviceDurations: this.state.bookingArray.serviceDurations,
        servicePrices: this.state.bookingArray.servicePrices,
        totalPrice: this.state.bookingArray.totalPrice,
        totalduration: this.state.bookingArray.totalduration,
      };
      console.log('all_service_details', service_details);
      this.props.setSelectedService(service_details);
      
      this.props.navigation.navigate('Masseuse');
    }
  };

  render() {
    const {checked} = this.state;
    console.log('render type states', this.states);

    //const {goBack} = this.props.navigation;
    const changeStyle =
      this.state.pressStatus === '0'
        ? [styles.greycard, styles.margingray, styles.active]
        : [styles.whitecard, styles.margingray, styles.active];
    const StaticStyle = [styles.graycard, styles.margingray, styles.active];
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('details')}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Available Services</Text>
          </View>
          {/* <TouchableOpacity style={styles.notify}>
            <Image
              source={require('../../assets/images/bell.png')}
              resizeMode="contain"
              style={{width: 40, height: 35}}
            />
          </TouchableOpacity> */}
        </View>
        <ScrollView>
          <TypeOfServices
            service={this.state.service}
            onSelectService={value => this.selectService(value)}
          />
          <View style={styles.container}>
            <Dialog
              visible={this.state.visible}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: 'bottom',
                })
              }
              onTouchOutside={() => {
                this.setState({visible: false});
              }}
              dialogStyle={{width: '80%'}}
              footer={
                <DialogFooter>
                  <DialogButton
                    textStyle={{
                      fontSize: 14,
                      color: '#333',
                      fontWeight: '700',
                    }}
                    text="OK"
                    onPress={() => {
                      this.setState({visible: false});
                    }}
                  />
                </DialogFooter>
              }>
              <DialogContent>
                <Text style={styles.popupText}>{this.state.popupMsg}</Text>
              </DialogContent>
            </Dialog>
          </View>
          <TypeOfDateTime
            onSelectDateTime={value => this.selectDateTime(value)}
          />

          <View style={styles.totalcount}>
            <Text style={[styles.h1, {marginBottom: 0}]}>Total: </Text>
            <Text style={styles.pinktext}>${this.state.totalprice}</Text>
          </View>

          <TouchableOpacity
            style={styles.pinkbtn}
            onPress={() => this.handleSubmit()}>
            <Text style={styles.btntext}>Book Appointment</Text>
          </TouchableOpacity>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.spa,
});
export default connect(mapStateToProps, {setSelectedService})(Typeof);
