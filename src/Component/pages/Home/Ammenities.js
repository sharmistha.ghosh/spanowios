import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import styles from './style';

class Ammenities extends Component {
  render() {
    const spaDescription = this.props.spaDescription;
    const count = this.props.count;
    const spaNoOfBeds = this.props.spaNoOfBeds;
    const spaIsParkingAvailable = this.props.spaIsParkingAvailable;
    const spaWifyAvailability = this.props.spaWifyAvailability;
    const spaAirConditionAvailability = this.props.spaAirconditionAvailable;
    const offer = this.props.offer;
    const offerMessege = this.props.offerMessege;


    return (
      <View>
        {offer ? 
          <View
            style={[styles.offerbtn, {flexDirection: 'row'}]}>
              <View style={{width: '25%', justifyContent: 'center', alignItems: 'center'}}>
                  <Image
                  source={require('../../assets/images/spacial-offer.png')}
                  resizeMode="contain"
                  style={{width: 82, height: 75 }}
                />
              </View>
            <View style={{width:'75%', justifyContent: 'center', alignItems: 'center'}}>
              <Text style={styles.offerText}>{offerMessege}</Text>
            </View>
          </View>
          : null
        }
        <View style={styles.graycard}>
          <Text style={styles.h2}>Description</Text>
          <Text style={styles.para}>{spaDescription}</Text>
        </View>

        <View style={styles.graycard}>
          <Text style={styles.h2}>Ammenities</Text>
          <View style={styles.paralist}>
            <Image
              source={require('../../assets/images/icon1.png')}
              resizeMode="contain"
              style={{width: 38, height: 34, marginRight: 10}}
            />
            <Text numberOfLines={1} style={styles.para}>
              {count} Masseuse
            </Text>
          </View>
          <View style={styles.paralist}>
            <Image
              source={require('../../assets/images/icon2.png')}
              resizeMode="contain"
              style={{width: 38, height: 34, marginRight: 10}}
            />
            <Text numberOfLines={1} style={styles.para}>
              {spaNoOfBeds} Beds
            </Text>
          </View>
          {spaIsParkingAvailable === '1' ? (
            <View style={styles.paralist}>
              <Image
                source={require('../../assets/images/icon3.png')}
                resizeMode="contain"
                style={{width: 38, height: 34, marginRight: 10}}
              />
              <Text numberOfLines={1} style={styles.para}>
                Self-Paking Available
              </Text>
            </View>
          ) : null}

          {spaWifyAvailability === '1' ? (
            <View style={styles.paralist}>
              <Image
                source={require('../../assets/images/icon4.png')}
                resizeMode="contain"
                style={{width: 38, height: 34, marginRight: 10}}
              />
              <Text numberOfLines={1} style={styles.para}>
                Complimentry WiFi
              </Text>
            </View>
          ) : null}

          {spaAirConditionAvailability === '1' ? (
            <View style={styles.paralist}>
              <Image
                source={require('../../assets/images/flake.png')}
                resizeMode="contain"
                style={{width: 38, height: 34, marginRight: 10}}
              />
              <Text numberOfLines={1} style={styles.para}>
              Air Conditioning
              </Text>
            </View>
          ) : null}
        </View>
      </View>
    );
  }
}

export default Ammenities;
