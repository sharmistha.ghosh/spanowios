import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert
} from "react-native";
import { Avatar, Icon, CheckBox } from "react-native-elements";
import styles from "./style";
import { connect } from "react-redux";
import { masseusePerSpa, allServiceNames, adminBookingFees, fetchSpecialOffer } from "./redux/spaAction";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import Spinner from "react-native-loading-spinner-overlay";
import { emptyStatement, thisTypeAnnotation } from "@babel/types";

class Messeuse extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      masseuseId: "",
      masseuseName: "",
      pressStatus: "0",
      itemNo: "-1",
      visible: false,
      popupMsg: "",
      spinner: true,
      specialOfferAvailable: false,
      specialDiscountPercent: ""
    };
    this.apiCallSpecialOffer();
  }

  apiCallSpecialOffer = () => {
    
    let spaid = this.props.selectedSpaDetails.spaId;
    let booking_date= this.props.selectedServiceDetails.date;

    let specialOffer = new FormData();
    specialOffer.append('spa_id', spaid);
    specialOffer.append('bookingDate', booking_date);
    console.log("apiCallSpecialOffer",specialOffer);
    this.props.fetchSpecialOffer(specialOffer);
  }

  //--SELECT NO MASSEUSE OPTION--//
  noMasseues = () => {
    this.setState({
      masseuseId: null
    });
    console.log("masseuse id", this.state.masseuseId);
  };

  //--SELECT MASSEUSE--//
  selectBox = (id, name) => {
    this.setState({
      itemNo: id,
      pressStatus: "1",
      masseuseId: id,
      masseuseName: name
    });
  };

  componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps", nextProps);
    this.setState({ spinner: false });
    if(nextProps.specialOffer.Ack == 1){
      this.setState({
        specialOfferAvailable: nextProps.specialOffer.special_discount.special_discount_Available,
        specialDiscountPercent: nextProps.specialOffer.special_discount.special_discount_Available ? nextProps.specialOffer.special_discount.special_discount : ""
      })
    }
  }

  //--FETCHING MASSEUSE OF SELECTED SPA USING REDUX--//
  componentDidMount() {
    let userDetails = new FormData();
    userDetails.append("spa_id", this.props.selectedSpaDetails.spaId);
    userDetails.append("booking_date", this.props.selectedServiceDetails.date);
    userDetails.append(
      "booking_start_time",
      this.props.selectedServiceDetails.time
    );
    userDetails.append(
      "booking_duration",
      this.props.selectedServiceDetails.totalduration
    );

    this.props.masseusePerSpa(userDetails);
    this.props.adminBookingFees();
  }

  //--SEND MASSEUSE ID, NAME TO THE NEXT PAGE USING PROPS--//
  handleSubmit = () => {
    var adminfee = "";
    if (this.state.masseuseId == "") {
      console.log("if in masseuse");

      this.setState({
        visible: true,
        popupMsg: "Either select any masuese or tick in No Choice"
      });
    } else {
      console.log("elses in masseuse");

      if(this.props.adminfee){
        adminfee = this.props.adminfee.admin_payment_percentage;
      }

      this.props.navigation.navigate("booking", {
        send_masseuseId: this.state.masseuseId,
        send_masseuseName: this.state.masseuseName,
        adminBookingFees: adminfee,
        specialOfferAvailable: this.state.specialOfferAvailable,
        specialDiscountPercent: this.state.specialDiscountPercent
      });
    }
  };

  render() {
    console.log(this.state.masseuseId);
    console.log("props value in masseuse page", this.props);

    const { goBack } = this.props.navigation;
    const changeStyle =
      this.state.pressStatus === "0" ? styles.graycard : styles.whitecard;
    const StaticStyle = styles.graycard;
    const itemNo = this.state.itemNo;

    // console.log("this.props.servicesNames[0].servicenames",this.props.servicesNames[0].servicenames);

    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="transparent" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity onPress={() => goBack()}>
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Available Masseuse</Text>
          </View>
          {/* <TouchableOpacity style={styles.notify}>
            <Image
              source={require('../../assets/images/bell.png')}
              resizeMode="contain"
              style={{width: 40, height: 35}}
            />
          </TouchableOpacity> */}
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={styles.spinnerTextStyle}
        />
        <ScrollView>
          <View style={styles.graycontainer}>
            {this.props.error && this.props.error !== undefined ? (
              Alert.alert("Something went wrong")
            ) : (
              <>
                {this.props.allMasseuse && this.props.allMasseuse.length ? (
                  <View>
                    {this.props.allMasseuse.map(item => {
                      return (
                        <TouchableOpacity
                          key={item.id}
                          //style={styles.graycard}
                          onPress={() =>
                            this.setState({
                              masseuseId: item.id,
                              masseuseName: item.name
                            })
                          }
                          style={itemNo === item.id ? changeStyle : StaticStyle}
                          onPress={() => this.selectBox(item.id, item.name)}
                        >
                          <View
                            style={[
                              styles.media,
                              { marginBottom: 0, width: "90%" }
                            ]}
                          >
                            {item.profile_image ? (
                              <Avatar
                                rounded
                                source={{ uri: `${item.profile_image}` }}
                                size={60}
                                containerStyle={{
                                  borderWidth: 1,
                                  borderColor: "#676782",
                                  padding: 6
                                }}
                              />
                            ) : (
                              <Avatar
                                rounded
                                source={require("../../assets/images/download.png")}
                                size={60}
                                containerStyle={{
                                  borderWidth: 1,
                                  borderColor: "#676782",
                                  padding: 6
                                }}
                              />
                            )}
                            <View style={styles.mediabody}>
                              <View style={styles.justifycontent}>
                                <Text style={styles.h3}>{item.name}</Text>
                              </View>
                              {/* {this.props.servicesNames ? (
                                <Text numberOfLines={1} style={styles.mutetext}>
                                  {this.props.servicesNames.services}
                                </Text>
                              ) : null} */}

                              {/* <TouchableOpacity>
                            <Text style={[styles.mutetext, {color: '#e25cff'}]}>
                              View Profile
                            </Text>
                          </TouchableOpacity> */}
                            </View>
                          </View>
                        </TouchableOpacity>
                      );
                    })}
                    <TouchableOpacity
                      style={[styles.pinkbtn, { marginTop: 50 }]}
                      onPress={this.handleSubmit}
                    >
                      <Text style={styles.btntext}>Book Masseuse</Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View style={styles.graycard}>
                    <Text style={styles.h3}>
                      SORRY. No masseuse available at this time.
                    </Text>
                    <Text style={styles.h3}>
                      Please go back and choose another time slot.
                    </Text>
                  </View>
                )}
              </>
            )}

            <View style={styles.container}>
              <Dialog
                visible={this.state.visible}
                dialogAnimation={
                  new SlideAnimation({
                    slideFrom: "bottom"
                  })
                }
                onTouchOutside={() => {
                  this.setState({ visible: false });
                }}
                dialogStyle={{ width: "80%" }}
                footer={
                  <DialogFooter>
                    <DialogButton
                      textStyle={{
                        fontSize: 14,
                        color: "#333",
                        fontWeight: "700"
                      }}
                      text="OK"
                      onPress={() => {
                        this.setState({ visible: false });
                      }}
                    />
                  </DialogFooter>
                }
              >
                <DialogContent>
                  <Text style={styles.popupText}>{this.state.popupMsg}</Text>
                </DialogContent>
              </Dialog>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.spa
});
export default connect(mapStateToProps, { masseusePerSpa, allServiceNames, adminBookingFees, fetchSpecialOffer })(
  Messeuse
);
