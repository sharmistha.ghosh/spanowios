import React, {Component} from 'react';
import {Text, View} from 'react-native';
import styles from './style';
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import {connect} from 'react-redux';
import {fetchServices, allServiceNames} from './redux/spaAction';

class TypeOfServices extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      serviceArray: [],
      arrServices: [],
      checked: '',
      service_id: [],
      service_name: [],
      service_price: [],
      service_duration: [],
      totalprice: 0,
      totalduration: 0,
      status: 1,
      service: this.props.service,
    };
    console.log('constructor', this.props);
  }

  //----FETCHING SERVICES OF SELECTED SERVICE USING REDUX----//
  componentDidMount() {
    console.log(this.props.selectedSpaDetails.spaId);
    let spaDetails = new FormData();
    spaDetails.append('spa_id', this.props.selectedSpaDetails.spaId);
    console.log('spaDetails', spaDetails);
    this.props.fetchServices(spaDetails);
    console.log('services', this.props.allServices);
  }

  //--------SELECT SERVICES------//
  onSelectservice(index, value) {
    if (this.state.status == 1) {
      if (this.props.allServices && this.props.allServices.length) {
        var services = this.props.allServices[0].service_name;
        for (var i = 1; i < this.props.allServices.length; i++) {
          services = services.concat(
            ',',
            this.props.allServices[i].service_name,
          );
        }
      }
      //console.log('services=========>>>>', services);
      this.props.allServiceNames({services});
      this.setState({state: 0});
    }

    console.log('index', index);
    console.log('value', value);

    //storing all selected service_id, service_name, service_duration, service_price//
    let service_object = {};
    let inner_service_json = {};
    let service_present = false;
    let arr_index = 0;
    let changed_array = {};

    let service_id = value.split('@')[0].trim();
    let service_name = value.split('@')[1].trim();
    let service_duration = value.split('@')[2].trim();
    let service_price = value.split('@')[3].trim();

    console.log('service_id', service_id);
    console.log('service_name', service_name);

    console.log('this.state.arrServices', this.state.arrServices);
    if (service_name == '') {
      console.log('service_name in if', service_name);

      if (this.state.arrServices.length) {
        changed_array = this.state.arrServices;

        for (let j = 0; j < changed_array.length; j++) {
          if (changed_array[j]['service_id'] == service_id) {
            // arr_index = i;
            changed_array.splice(j, 1);
            break;
          }
        }

        this.setState({
          arrServices: changed_array,
        });
      }
    } else {
      inner_service_json['service_id'] = service_id;
      inner_service_json['service_name'] = service_name;
      inner_service_json['service_duration'] = service_duration;
      inner_service_json['service_price'] = service_price;

      console.log('inner_service_json', inner_service_json);

      service_object['service_id'] = service_id;
      service_object['service_data'] = inner_service_json;

      console.log('service_object', service_object);

      console.log('this.state.arrServices', this.state.arrServices);
      console.log('service_name', service_name);

      for (let i = 0; i < this.state.arrServices.length; i++) {
        if (this.state.arrServices[i]['service_id'] == service_id) {
          service_present = true;
          arr_index = i;
          break;
        }
      }
      if (!service_present) {
        this.state.arrServices.push(service_object);
      } else {
        let stored_array = this.state.arrServices;
        stored_array[arr_index]['service_data'] = inner_service_json;
        this.setState({
          arrServices: stored_array,
        });
      }
    }

    console.log('after update this.state.arrServices', this.state.arrServices);

    let prices = 0;
    let durations = 0;
    let serid = [];
    let serduration = [];
    let serprice = [];
    let sername = [];

    //----calculating total service price----//
    for (let i = 0; i < this.state.arrServices.length; i++) {
      prices += parseFloat(
        this.state.arrServices[i]['service_data'].service_price,
      );
    }
    this.setState({totalprice: prices});

    //----calculating total service duration----//
    for (let i = 0; i < this.state.arrServices.length; i++) {
      durations += parseFloat(
        this.state.arrServices[i]['service_data'].service_duration,
      );
    }
    this.setState({totalduration: durations});

    //select all serviceids, service name, service duration, service price//
    for (let i = 0; i < this.state.arrServices.length; i++) {
      serid.push(this.state.arrServices[i]['service_data'].service_id);
      serduration.push(
        this.state.arrServices[i]['service_data'].service_duration,
      );
      serprice.push(this.state.arrServices[i]['service_data'].service_price);
      sername.push(this.state.arrServices[i]['service_data'].service_name);
    }

    let service_details = {
      serviceIds: serid.toString(),
      serviceNames: sername.toString(),
      serviceDurations: serduration.toString(),
      servicePrices: serprice.toString(),
      totalPrice: prices,
      totalduration: durations,
    };
    console.log('all_service_details', service_details);
    this.setState({
      serviceArray: service_details,
    });
    this.props.onSelectService(service_details);
  }

  render() {
   // const {checked} = this.state;
    return (
      <View>
        {this.state.service && this.state.service.length ? (
          <View style={styles.graycontainer}>
            {this.state.service.map((item, key) => (
              <View style={styles.graycard}>
                <Text style={styles.h2}>{item.service_name}</Text>

                <RadioGroup
                  onSelect={(index, value) =>
                    this.onSelectservice(index, value)
                  }>
                  {item.price_duration.map((item1, key1) => (
                    <RadioButton
                      value={
                        item.service_id +
                        '@' +
                        item.service_name +
                        '@' +
                        item1.duration +
                        '@' +
                        item1.price
                      }>
                      <Text style={{color: '#e25cff'}}>
                        {'$' + item1.price + '/' + item1.duration + 'min'}
                      </Text>
                    </RadioButton>
                  ))}

                  <RadioButton
                    value={item.service_id + '@' + '' + '@' + '' + '@' + ''}>
                    <Text style={{color: '#e25cff'}}>None</Text>
                  </RadioButton>
                </RadioGroup>
              </View>
            ))}
          </View>
        ) : (
          <View>
            <Text style={styles.h1}>
              This Spa services are not available right now
            </Text>
          </View>
        )}
      </View>
    );
  }
}
const mapStateToProps = state => ({
  ...state.spa,
});
export default connect(mapStateToProps, {fetchServices, allServiceNames})(
  TypeOfServices,
);
