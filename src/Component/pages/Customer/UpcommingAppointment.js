import React, { Component } from "react";
import {
  Text,
  View,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
  Image,
  AsyncStorage,
  Alert
} from "react-native";
import { Avatar, Icon, CheckBox } from "react-native-elements";
import styles from "./style";
import Dialog, {
  SlideAnimation,
  DialogButton,
  DialogContent,
  DialogFooter
} from "react-native-popup-dialog";
import { connect } from "react-redux";
import {
  fetchUpcommingAppointment,
  cancelBooking,
  fetchCustomerDetails
} from "./redux/customerAction";
import moment from "moment";
import Spinner from "react-native-loading-spinner-overlay";

class UpcommingAppointment extends Component {
  static navigationOptions = {
    header: null
  };
  constructor(props) {
    super(props);
    this.state = {
      customer_id: "",
      visible1: false,
      visible2: false,
      popupMsg: "",
      popupMsg1: "",
      bookingId: "",
      spinner: true,
      status: ""
    };
    todayDate = moment(new Date()).format("YYYY-MM-DD");
  }

  componentWillReceiveProps(nextProps) {
    console.log("componentWillReceiveProps", nextProps);
    this.setState({ spinner: false });
  }

  componentDidUpdate(prevProps) {
    console.log("componentDidUpdate====", this.props);

    if (prevProps.cancelBookings !== this.props.cancelBookings) {
      this.setState({ spinner: false });
      console.log("if2");

      const addDetails = this.props.cancelBookings;
      if (this.props.error && this.props.error !== undefined) {
        console.log("if2");

        Alert.alert("Something went wrong.");
      } else {
        console.log("else1");

        if (addDetails.Ack === 1) {
          this.setState({
            status: 1
          });
          //Alert.alert(addDetails.msg);
          //this.props.navigation.navigate("myAccount");
          this.setState({
            visible2: true,
            popupMsg1: addDetails.msg
          });
        } else {
          Alert.alert("Refund Process. Something went wrong.");
        }
      }
    }
  }

  //--FETCH UPCOMMING APPOINTMENT THROUGH REDUX--//
  async componentDidMount() {
    console.log("componentDidMount in myaccount page");

    this.props.navigation.addListener("willFocus", () => {
      this.setState({ spinner: true });
      console.log("focus");
      if (this.state.customer_id) {
        let currenttime = moment(new Date()).format("HH:MM:ss");
        let userDetails = new FormData();
        userDetails.append("customer_id", this.state.customer_id);
        userDetails.append("todaysDate", todayDate);
        userDetails.append("time", currenttime);
        console.log("focused working",userDetails);
        this.props.fetchUpcommingAppointment(userDetails);

        let userDetails2 = new FormData();
        userDetails2.append("user_id", this.state.customer_id);
        this.props.fetchCustomerDetails(userDetails2);
      }
    });

    try {
      const value = await AsyncStorage.getItem("login_id_2");
      console.log("loginId in myaccount page", value);
      if (value) {
        this.setState({ customer_id: value });
      }
    } catch (error) {
      console.log(error);
      // Error retrieving data
    }
    let currenttime = moment(new Date()).format("HH:MM:ss");
    let userDetails = new FormData();
    userDetails.append("customer_id", this.state.customer_id);
    userDetails.append("todaysDate", todayDate);
    userDetails.append("time", currenttime);
    console.log("userDetails==========", userDetails);

    this.props.fetchUpcommingAppointment(userDetails);

    let userDetails2 = new FormData();
    userDetails2.append("user_id", this.state.customer_id);
    this.props.fetchCustomerDetails(userDetails2);
  }

  closePopupbox1 = () => {
    this.setState({
      visible2: false
    });

    if (this.state.status == 1) {
      this.props.navigation.navigate("myAccount");
    }
  };

  cancelBookingId = bookingId => {
    console.log("cancelBooking", bookingId);

    if (this.state.visible1 === false) {
      this.setState({
        visible1: true,
        popupMsg: "Are You Sure You Want To Cancel This Booking",
        bookingId: bookingId
      });
    }

    // let userDetails = new FormData();
    // userDetails.append('id', id);
    // this.props.cancelBooking(userDetails);
    // // this.props.navigation.navigate('MasseuseList');
    // console.log('cancelBooking function', this.props.masseusedelete);
  };

  cancelAppointment = () => {
    console.log("visible value", this.state.visible1);

    if (this.state.visible1 == true) {
      this.setState({ visible1: false, spinner: true });
      let currenttime = moment(new Date()).format("hh:mm:ss");

      let cancelData = new FormData();
      cancelData.append("booking_id", this.state.bookingId);
      cancelData.append("todaysDate", todayDate);
      cancelData.append("time", currenttime);
      console.log("cancelBooking payload", cancelData);

      this.props.cancelBooking(cancelData);
    }
  };

  completePaymentNow = (booking_id) => {
    console.log("booking_id",booking_id);
    this.props.navigation.navigate("CardFormScreen",{booking_id : booking_id})
  };

  //--CLOSE POPUP BOX--//
  closePopupbox = () => {
    console.log("visible value", this.state.visible1);

    if (this.state.visible1 == true) {
      this.setState({ visible1: false });
    }
  };

  render() {
    console.log("props in render in upcomming appointment", this.props);
    console.log(
      "props in render in upcomming appointment",
      this.props.upcommingAppointment
    );

    //const {goBack} = this.props.navigation;
    return (
      <SafeAreaView style={styles.graybody}>
        <StatusBar barStyle="light-content" backgroundColor="#1a1a1d" />
        <View style={[styles.topbar]}>
          <View style={styles.justifyrow}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("myAccount")}
            >
              <Icon name="md-arrow-back" type="ionicon" color="#fff" />
            </TouchableOpacity>
            <Text style={styles.backheading}>Upcoming Appointments</Text>
          </View>
        </View>
        <Spinner
          visible={this.state.spinner}
          textContent={"Loading..."}
          textStyle={styles.spinnerTextStyle}
        />
        <ScrollView>
          {/* {this.props.error && this.props.error !== undefined ? (
            Alert.alert('Something went wrong')
          ) : ( */}
          <View style={styles.graycontainer}>
            {this.props.upcommingAppointment.length ? (
              this.props.upcommingAppointment.map((item, key) => (
                <View key={key} style={styles.graycard}>
                  <View
                    style={[styles.media, { marginBottom: 0, width: "90%" }]}
                  >
                    <Avatar
                      rounded
                      source={{
                        uri: `${this.props.customerDetails.profile_image}`
                      }}
                      size={60}
                      containerStyle={{
                        borderWidth: 3,
                        borderColor: "#676782",
                        padding: 3,
                        backgroundColor: "#fff"
                      }}
                    />
                    <View style={styles.mediabody}>
                      <Text style={styles.h3}>{item.spa_name}</Text>
                      <View
                        style={[
                          styles.justifyrow,
                          { marginTop: 5, flexWrap: "wrap" }
                        ]}
                      >
                        <View style={styles.justifyrow}>
                          <Image
                            source={require("../../assets/images/calender.png")}
                            resizeMode="contain"
                            style={{ width: 14, marginRight: 5 }}
                          />
                          <Text style={styles.mutetext2}>
                            {item.booking_date}
                          </Text>
                        </View>
                        <View style={styles.justifyrow}>
                          <Image
                            source={require("../../assets/images/clock.png")}
                            resizeMode="contain"
                            style={{ width: 14, marginRight: 5 }}
                          />
                          <Text style={styles.mutetext2}>
                            {item.booking_start_time}
                          </Text>
                        </View>
                        <View style={styles.justifyrow}>
                          <Image
                            source={require("../../assets/images/setting.png")}
                            resizeMode="contain"
                            style={{ width: 14, marginRight: 5 }}
                          />
                          <Text style={styles.mutetext2}>{item.services}</Text>
                        </View>
                        <View style={styles.justifyrow}>
                          <Image
                            source={require("../../assets/images/clock.png")}
                            resizeMode="contain"
                            style={{ width: 14, marginRight: 5 }}
                          />
                          <Text style={styles.mutetext2}>
                            Duration: {item.total_duration} min
                          </Text>
                        </View>
                        {item.masseuse_name ? (
                          <View style={styles.justifyrow}>
                            <Image
                              source={require("../../assets/images/picon1.png")}
                              resizeMode="contain"
                              style={{ width: 14, marginRight: 5 }}
                            />
                            <Text style={styles.mutetext2}>
                              {item.masseuse_name}
                            </Text>
                          </View>
                        ) : null}

                        <View>
                          <View style={styles.container}>
                            <Dialog
                              visible={this.state.visible2}
                              dialogAnimation={
                                new SlideAnimation({
                                  slideFrom: "bottom"
                                })
                              }
                              onTouchOutside={() => {
                                this.closePopupbox1();
                              }}
                              dialogStyle={{ width: "80%" }}
                              footer={
                                <DialogFooter>
                                  <DialogButton
                                    textStyle={{
                                      fontSize: 14,
                                      color: "#333",
                                      fontWeight: "700"
                                    }}
                                    text="OK"
                                    onPress={() => {
                                      this.closePopupbox1();
                                    }}
                                  />
                                </DialogFooter>
                              }
                            >
                              <DialogContent>
                                <Text style={styles.popupText}>
                                  {this.state.popupMsg1}
                                </Text>
                              </DialogContent>
                            </Dialog>

                            <Dialog
                              visible={this.state.visible1}
                              dialogAnimation={
                                new SlideAnimation({
                                  slideFrom: "bottom"
                                })
                              }
                              onTouchOutside={() => {
                                this.closePopupbox();
                              }}
                              dialogStyle={{ width: "80%" }}
                              footer={
                                <DialogFooter>
                                  <DialogButton
                                    textStyle={{
                                      fontSize: 14,
                                      color: "#333",
                                      fontWeight: "700"
                                    }}
                                    text="Yes"
                                    onPress={() => {
                                      this.cancelAppointment();
                                    }}
                                  />
                                  <DialogButton
                                    textStyle={{
                                      fontSize: 14,
                                      color: "#333",
                                      fontWeight: "700"
                                    }}
                                    text="No"
                                    onPress={() => {
                                      this.closePopupbox();
                                    }}
                                  />
                                </DialogFooter>
                              }
                            >
                              <DialogContent>
                                <Text style={styles.popupText}>
                                  {this.state.popupMsg}
                                </Text>
                              </DialogContent>
                            </Dialog>
                          </View>
                          {item.payment_status == 1 ? null : (
                            <View style={styles.justifyrow}>
                              <TouchableOpacity
                                onPress={this.completePaymentNow.bind(this, item.id)}
                                style={[
                                  styles.smbtn,
                                  { backgroundColor: "#e25cff" }
                                ]}
                              >
                                <Text
                                  style={[styles.smtext, { color: "#ffffff" }]}
                                >
                                  Payment Due
                                </Text>
                              </TouchableOpacity>
                            </View>
                          )}
                          {item.payment_method == "Card" ? (
                            <TouchableOpacity
                            onPress={this.cancelBookingId.bind(this, item.id)}
                            style={[
                              styles.smbtn,
                              { backgroundColor: "#ffffff" }
                            ]}
                          >
                            <Text style={[styles.smtext, { color: "#e25cff" }]}>
                              Cancel Booking
                            </Text>
                          </TouchableOpacity>
                          ) : null}       
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              ))
            ) : (
              <Text style={styles.backheading}>
                No Upcoming Appointment Found
              </Text>
            )}
          </View>
          {/* )} */}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const mapStateToProps = state => ({
  ...state.customer
});
export default connect(mapStateToProps, {
  fetchUpcommingAppointment,
  cancelBooking,
  fetchCustomerDetails
})(UpcommingAppointment);
